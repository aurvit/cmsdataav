**07/19**

Sutvarkiau dokumentus reikalingus darbintis, rytoj juos išsiųsiu. Taip pat bandžiau instaliuotis virtualią mašiną. Kaltinsiu savo neatidumą ir pdf failo apgavystes, kad nepastebėjau, kad skaičiau ne priedą. Instaliuotis nepavyko, tęsiu rytoj.

**07/20**

Dokumentai buvo išsiųsti be jokių problemų. Pradėjau diegtis mašiną. Nežinau, kas ten buvo, reikėjo atjungti vieną mygtuką, kad pagaliau importuotųsi, bet taip ir nesupratau ar tai gerai. Viskas po to ėjo normaliai, tik patikrinimo negali praeiti. Atidaryti failą atidaro, bet negali nuskaityti, neranda plugino.

Sutvarkiau visą įdiegimą ir pirma dalis - su terminalu - pagaliau pasibaigė teigiamai. Problema buvo tai, kad ne tą terminalą naudojau. Čia aš išdėstysiu žingsnius, kaip patikrinti terminalu virtualią mašiną:

1. Parsisiunčiama Oracle VM virtual box

2. Tada iš cern open data website, parsisiunčiama CMS-OpenData-1.5.2 (nes aš tokią naudojau, apie tai kiek vėliau)

3. Atidarome ką tik parisisiųstą failą ir išmes Oracle VM programos lentelė (bent turėtų. Jei neišmeta, tada per Oracle reikės spausti import ir pasirinkti failą.). Joje nieko nekeičiama tik tai reikia atžymeti apačioje Import hard drives as VMI, nes tai neleis importuoti. Tada viskas turėtų sklandžiai susiimportuoti.

4. Tada bus Oracle lange šone ta VM su prierašu Powered down. Ant jos paspaudžiama ir spaudžiame settings. Ten Display panel, pasirenkame VMSVGA graphics controllerį ir taip pat, kad būtų galima matyti vaizdą  remote display panel paspausti enable remote server (arba kažką panašaus).

5. Tada spaudžiame prie start mygtuko normal start (po varnele) ir turėtų įsijungti virtuali mašina, gali užtrukti. Jei neįsijungia dėl network, adapter 2 pakeisti į NAT port

6. Tada reikia ją patikrinti. Jeigu versija yra kitokia nei mano, reiktų tada tiesiog spausti read me failą ir skaitytis, nes senesnės versijos galėjo naudotis terminalu, kuris yra Outer shell, mano versijoje and dekstopo reikia atsidaryti CMS shell programą (turėtų būti apatinė programa, bet ne shortcut bar).

7. Tada reikia rašyti kodus vienas po kito:

   1. cmsrel CMSSW_5_3_32 (tai sukurs aplinką ir po to niekada nebereikės rašyti)
   2. cd CMSSW_5_3_32/src/ (pakeis direktoriją)
   3. cmsenv (leidžia naudotis cms komandomis ir galios, kol neišjungsite terminalo)
   4. mkdir Demo (sukurs tokią direktoriją)
   5. cd Demo
   6. mkedanlzr DemoAnalyzer (sukurs tikrinimo kodą)
   7. cd DemoAnalyzer
   8. scram b (sukompiliuos sukurtą kodą)

8. Tada reikia nueiti į direktoriją DemoAnalyzer (ne per terminalą, jo nereikia išjungti) ir susirasti failą demoanalyzer_cfg.py. Jį atsidarius kode surandama file:myfile.root eilutė ir pakeičiama į root://eospublic.cern.ch//eos/opendata/cms/Run2011A/ElectronHad/
   AOD/12Oct2013-v1/20001/001F9231-F141-E311-8F76-003048F00942.root. O eilutėje process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) -1 pakeičiamas į 10

9. Tada terminale ar CMS Shell programoje įrašomas kodas cd ../.., tai sugrąžins mus į senesnes direktorijas.

10. Paleiždiamas kodas ar programa cmsRun Demo/DemoAnalyzer/demoanalyzer_cfg.py ir jei gaunamas vaizdas panašus į tuoj būsimą vaizdą, tai terminalas sėkmingai veikia:

    02-Oct-2018 00:33:54 EEST Initiating request to open file
    root://eospublic.cern.ch//eos/opendata/cms/Run2011A/ElectronHad/
    AOD/12Oct2013-v1/20001/001F9231-F141-E311-8F76-003048F00942.root
    02-Oct-2018 00:34:11 EEST Successfully opened file
    root://eospublic.cern.ch//eos/opendata/cms/Run2011A/ElectronHad/
    AOD/12Oct2013-v1/20001/001F9231-F141-E311-8F76-003048F00942.root
    Begin processing the 1st record. Run 166782,
    Event 340184599, LumiSection 309 at 02-Oct-2018 00:34:22.339
    EEST Begin processing the 2nd record. Run 166782,
    Event 340185007, LumiSection 309 at 02-Oct-2018 00:34:22.340
    EEST Begin processing the 3rd record. Run 166782,
    Event 340187903, LumiSection 309 at 02-Oct-2018 00:34:22.341
    EEST Begin processing the 4th record. Run 166782,
    Event 340227487, LumiSection 309 at 02-Oct-2018 00:34:22.341
    EEST Begin processing the 5th record. Run 166782,
    Event 340210607, LumiSection 309 at 02-Oct-2018 00:34:22.342
    EEST Begin processing the 6th record. Run 166782,
    Event 340256207, LumiSection 309 at 02-Oct-2018 00:34:22.342
    EEST Begin processing the 7th record. Run 166782,
    Event 340165759, LumiSection 309 at 02-Oct-2018 00:34:22.342
    EEST Begin processing the 8th record. Run 166782,
    Event 340396487, LumiSection 309 at 02-Oct-2018 00:34:22.343
    EEST Begin processing the 9th record. Run 166782,
    Event 340390767, LumiSection 309 at 02-Oct-2018 00:34:22.343
    EEST Begin processing the 10th record. Run 166782,
    Event 340435263, LumiSection 309 at 02-Oct-2018 00:34:22.344
    EEST 02-Oct-2018 00:34:22 EEST Closed file
    root://eospublic.cern.ch//eos/opendata/cms/Run2011A/ElectronHad/
    AOD/12Oct2013-v1/20001/001F9231-F141-E311-8F76-003048F00942.root

11. 

Su tuo galima pereiti į root tikrinimą. Ką aš darysiu rytoj

**08/03:**

*Pradedama tikrinti straipsnius.*

**Pirmas straipsnis**

**Study of the Higgs boson produced in association with a weak boson and decaying to WW* with a same sign dilepton and jets final state in sqrt(s)=8TeV PP collisions with the ATLAS detector at the LHC**

216 puslapiu. Pabandysiu kuo lengviau ir greiciau perskaityti, bet bijau, kad uztruksiu.

Kolkas per didelis straipsnis, pabandysiu veliau

**Antras straipsnis**

**New results on W boson production with the ATLAS detector**

Straipsnis yra 4 puslapiu, zymiai trumpesnis.

Straipsnyje nera pazymeta, kur yra kokie jets, bet kalba eina apie koregavima W bozonui rasti, tai atrodo, kad gali tikti.

**Trecias straipsnis**

**New Results on W Boson production and multi-lepton cross sections with the ATLAS detector**

Straipsnis net ne straipsnis, o powerpoint presentation. Bet gal bus usefull.

Kalba eina apie muonu produkcija, ne elektronu, kas nera reikalinga.

**Ketvirtas straipsnis**

**New results on W boson production with the ATLAS detector**

Straipsnis ir vel powerpoint.

Vis del to sitame straipsnyje buvo daugiau apie jets, taciau duomenu susijusiu su jais nelabai buvo.

**Penktas straipsnis**

**Measurment of the top quark mass using lepton transverse momenta with the ATLAS detector sqrt(s)=8TeV**

115 puslapiu. Gal ir vel paliksiu velesniam laikui.

Sitame straipsnyje yra nuorods i data lists, kas yra labai gerai. Tik dar reikes mismokti apdoroti tuos duomenis reikiamai. Taip pat reikia pazymeti, kad sitame straipsnyje yra ieskoma top kvarko mase, kas gali kaip nors prisideti prie duomenu analyzes.

**Sestas straipsnis**

**Combination of the W boson polarization measurments in top quark decays using ATLAS and CMS data at 8 TeV**

Straipsnis vel powerpoint.

Sitas kiek keblesnis. Jis matuoja W bozono ir vieno leptono atsiradima skilus top kvarkui. Yra pazymeta, kad yra W bozonas ir dileptonas (straipsnio nenaudojamas) tai tada kyla klausimas ar sitas vertas kazko straipsnis. Kolkas priimsiu, kad verytas.
*Septintas straipsnis pasirodo buvo straipsnis, pagal kuri buvo padarytas sestas straipsnis. Ten duomenys nerasti.*

**Astuntas straipsnis*

**Measurement of the associated production of a W boson and a charm quartk at sqrt(s)=8TeV**

Mazai tikimybes, kad cia bus kalba apie  dileptonus, bet gal bus.

Tikimybe buvo maza, bet nepasiteisino ji. Matuojamas W bozonas per elektrona (muona) ir neutrina, ko mums nereikia.

**Devintas straipsnis**

**Measurement of single top-quark production in association with a W boson in the single-lepton channel at sqrt(s)=8TeV with the ATLAS detector**

Nusimato, kad tikrai nebus cia nieko.

Sitas kiek keistas. Jis kalba apie skylima leptonini ir atrodo, kad gali buti tikrinamas W bozonas pagal dileptonini skylima. Bet tuo paciu nelabai yra tam duomenu, tik atrinkimas elektronu.

**Desimtas straipsnis**

**Measurement of the production cross section of a W boson in association with two b jets in pp collisions at sqrt(s)=8TeV**

b jets man atrodo netiks.

Kaip ir sakiau, netinka, nes W bozonas atrenkamas pagal single lepton and neutrino.

**Vienuoliktas straipsnis**

**Searches for supersymmetry based on events with b jets and four W bosons in pp collisions at 8TeV**

Atrodo labiau promising, nes atrodo, kad apie visus channels kalbes

Yra kalba apie dilepton tikrinima, kuris yra menkas. Yra references i dilpetons. Gali buti labai useful.

**Dvyliktas straipsnis**

**Measurement of the W boson helicity in events with a signle reconstructed top quark in pp collisions at sqrt(s)=8TeV**

cia W bozonas skyla i viena leptona ir neutrino. Tai nera svarbu

**Tryliktas straipsnis**

**Search for massive resonances decaying into pairs of boosted bosons in semi-leptonic final states at sqrt(s)=8TeV**

Gaila, bet sitame straipsnyje W bozonas tikrinamas pagal skylima i elektrona ir neutrina.

Straipsniai buvo rasti pasitelkus raktiniais zodziais W boson, jets, pp, 8 TeV. Po to juos atrinkau paskaitydamas abstraktus ir ieskodamas, kur tiksliau kalba apie W bozono skylima ir detection. Liko dar vienas didelis straipsnis, kuri dabar perziuresiu.

**Pirmas straipsnis (is naujo)**

Del laiko stokos as neskaitysiu viso straipsnio, labiau stengsiuosi atrasti, kur yra kalba apie W bozono detection. Is abstrakto as pastebiu, kad kalba eina apie same charge leptons, kas kelia vilciu, kad sitame straipsnyje reiktu gyliau pasikapstyti.

Na sunku pasakyti ar tai yra geras straipsnis. Yra matuojami channels dilepton, bet taip pat sunku pasakyti, kad ten viskas ko reikia, nes ten techniskai kalba eina apie Higso bozono skylima ir W bozonus ir tada ju skylima i leptonus (ir du kvarkus), kas nezinau kiek yra tai ko, ieskau. Bet straipsnis tuo paciu atrodo svarbus ir ji labiau isstudijavus butu gal geriau nusakyta jo svarba, bet bijau, kad as nesu tiek gerai su visa metodologija susipazines, kad galeciau pilnai suprasti, kas ten parasyta.

**08/04:**

Kadangi aš blogai pagalvojau, kad W bozonas turėjo skilti tik į elektroną ir elektroną (reikia leptono ir neutrino), peržiūriu iš naujo atmestus straipsnius.

**Trecias straipsnis (is naujo)**

powerpointas, kuris man atrodo remiasi duomenimis is pirmo straipsnio. Manau, kad verta perziureti, nes yra kalbama apie skilimus, kurie reikalingi.

**Astuntas straipsnis (is naujo)**

Perziurejus atrinkinejamas W bozonas per lV skylima, kas yra reikalinga.

**Devintas straipsnis (is naujo)**

Kalba eina apie skylima i lV tai manau tinka

**Dvyliktas straipsnis (is naujo)**

Nelabai zinau ar jis tinka. Vienoje vietoje, apie atranka autorius kalba, kad neutrinas nedetectinamas todel ten imbalance atsiranda, taciau abstrakte raso apie single lepton ir two jets. Del sito nelabai zinau.

**Tryliktas straipsnis (is naujo)**

Yra kalba apie reikiama skylima, manau tinka

**Keturioliktas straipsnis**

**Measurement of electroweak production of a W boson and two forward jets in proton-proton collisions at sqrt(s)=8TeV**

Nusimato, kad gal bus, ko reikia

Gali buti naudingas

**Penkioliktas straipsnis**

**Search for heavy Majorana neutrinos in e+/-e+- plus jets and e+/-mu+/- plus jets events in proton-proton collisions at sqrt(s)=8TeV**

Eina kalba apie Majorana neutrino skylima i W bozona, bei energy range yra geras. Taciau, kad ir yra reikiamu dalyku, cia W bozonas skyla i qq, tai nesvarbu.

**Sesioliktas straipsnis**

**Improving Constrains on Proton Structure using CMS measurements**

Gal bus kazko naudingo.

Nors pasakyta, kad yra range 7 ir 8 TeV, vis del to yra tik 7 ir nelabai parasyta apie W bozona ir jo duomenis

**Septynioliktas straipsnis**

**Search for a massive resonance decaying into a Higgs boson and aW or Z boson in hadronic final states in proton-proton collisions at sqrt(s)=8TeV**

Stebes W bozona, gal bus apie ji daugiau kalbama

Vis delto netinka, nes stebimas W bozonas skyla i qq

**Astuonioliktas straipsnis**

**Study of vector boson scattering and search for new physics in events with tow same sign leptons and two jets**

Gal bus kazkas reikalingo

Atrodo, kad gal yra kazko svarbaus, bet tuo paciu labai keistai parasytas straipsnis, bet gal yra is to naudos.

**Devynioliktas straipsnis**

**Search for anomalous gauge couplings in semi-leptonic decays of WWgamma and WZgamma in pp collisions at sqrt(s)=8TeV**

Neradau is abstrakto, kaip stebi W bozona, bet gal bus kas nors gero.

Atrodo, kad yra kalbos apie reikiama skylima, tai gal tiks.

**Dvidesimtas straipsnis**

**Opportunities and Challenges of Standard Model production cross section measurements at 8 TeV using CMS Open Data**

Gal bus kazkas

Atrodo, kad yra kazkas susije su W bozonu ir reikiamu skylimu.

**Dvidesimt pirmas straipsnis**

**A study of the PDF uncertainty on the LHC W-boson mass measurement**

Susijes su W bozono mases matavimais, gal tiks

Matavimai parodyti tik 7TeV srityje, bet gale pasako, kad 8 ir 13 TeV srityse gaunami panasus rezultatai, gal visgi tiks.

**Dvidesimt antras straipsnis**

**Combination of searches for heavy resonances decaying to WW, WZ,ZZ, WH, and ZH boson pairs in proton-proton collisions at sqrt(s)=8 and 13TeV**

Nezinau, kaip gerai cia, bet stebejo W bozonus, tai gal bus gerai.

Vis delto netinka, nes stebejimas yra W i qq
WplusminusZ production at the LHC: fiducial cross sections and distributions in NNLO QCD
Is abstrakto matosi, kad kalba eisi apie leptonini skylima

**08/05**

Čia aš pradėsiu straipsnių skaitymą. Pradėsiu nuo powerpointų ir trumpiausių straipsnių. Jų pavadinimus ir linkus užrašysiu.

**[New Results on W Boson production and multi-lepton cross sections with the ATLAS detector](http://cds.cern.ch/record/2644311?ln=en)**

Parametrai:
E(miss)>25GeV

m(T)>40GeV

20.2fb^-1

R=0,4

p(jet)>30GeV

y(jet)<4,4.

Perskaitęs skaidres, pasidarė kiek aiškiau, apie ką tekstas. Tekste buvo apdoroti duomenys ir nagrinėti pdf'ai, kaip jie modeliuoja duomenis. Taip pat buvo papildomų duomenų, kai sqrt(s)=7 ar 13 TeV, kas nesvarbu. Parametrus, kuriuos radau, buvo pačioje pradžioje, bei atrodė, kad aiškaus jų paaiškinimo nebuvo.

**[New results on W boson production with the ATLAS detector](http://cds.cern.ch/record/2649300?ln=en)**

Pasirodo aš šito straipsnio skaidrių nesisiunčiau, nes jau turėjau straipsnį.

20.2 fb^-1

pT>20GeV

|eta|<2,47

E(miss)>25GeV

mT>40GeV

R=0,4

|y|<4,4


Perskaičius, prisiminiau, kad rašytis labai daug apie ATLAS, gal nėra gerai, tačiau, jei aš gerai prisimenu, tai CMS turėjo panašius atrinkimo parametrus, tai teks vėliau patikrinti.

**[Combination of the W boson polarization measurements in top quark decays using ATLAS and CMS data @ 8 TeV](http://cds.cern.ch/record/2722659?ln=en)**

20,2 fb^-1

19,8 fb^-1 CMS

Peržiūrėjus skaidres, pastebėtas skirtumas, kad ir menkas, tarp CMS ir ATLAS skaičių, atrankos parametrų. Tačiau problema yra didesnė, kai nėra duoti jokie kiti parametrai apart, kuriuos užrašiau. Taip pat, nėra parodyta, iš kur imti duomenys, arba aš tiesiog nepamačiau. Todėl šitas straipsnis ko gero blogokas.

**[Searches for supersymmetry based on events with b jets and four W bosons in pp collisions at 8 TeV](https://arxiv.org/abs/1412.4109v2)**

19,5 fb^-1

R=0,5
*Triple object trigger*:
pT>15GeV

Emiss>45GeV

HT>350GeV
*Double object trigger:*

no Emiss

**pT threshold of 40 GeV**. The data samples for the dilepton and multilepton analyses were collected with ee, em, and mm double-lepton triggers, which require at least one e or one m with **pT > 17 GeV** and another with **pT > 8 GeV.**

*Fully hadronic analysis*:

requires three jets

pT>50GeV

|eta|<2,5

veto for single lepton pt>10GeV and |eta|<2,5;2,4

The HT and Hmiss T values are required to exceed 500 and 200 GeV, respectively

Njets = (3–5), (6–7), and >=8.
The events are further divided into exclusive regions of HT and Hmiss

*Single lepton analysis:*

single electron or muon pT>20GeV and no additional lepton with pT>15GeV

same |eta| restrictions as in previous section

Events must satisfy Njets >= 6, Nbjets >= 2, HT > 400 GeV, and Slep > 250 GeV

Čia taip pat pridedama daugiau parametrų, bet jie labiau yra susiję su SUSY, kas čia nebus nagrinėjama, tai jų nepridedu.

*Same-sign dilepton:*

pT>20GeV

|eta|<2,4

veto leptons pT>10GeV

jets pT>40GeV

Njets > 2, HT > 200 GeV, and EmissT >50 GeV

*Multilepton analysis:*

pT>10GeV except one lepton must have pT>20GeV

|eta|<2,4

jets pT>30GeV

Events must satisfy Njets >= 2, Nbjets >= 1, HT > 60 GeV, and EmissT > 50 GeV

*Opposite sign dilepton analysis:*

each letpon pT>20GeV

|eta|<2,4

third lepton veto pT>20GeV and |eta|<2,4

jets satisfy pT>30GeV and |eta|<2.5


Tekste yra daug visokių parametrų, pasistengiau juos visus surašyti, bet sunkoka yra pasakyti, kas tikrai yra naudinga. Kaip suprantu mums užtenka tik single lepton analysis, bet dėl viso pikto aprašiau ir kitus (gal kam kitam prireiks vėliau). Masės parametrų nelabai radau, nors nežinau ar tokių reikia. Visus references perkėliau čia, nes nežinau, kurie tiksliai yra reikalingi.

**[Measurement of single top-quark production in association with a W boson in the single-lepton channel at s√=8 TeV with the ATLAS detector](https://arxiv.org/abs/2007.01554v1)**

Kadangi čia Atlas Collaboration aš parametrų daugiau nerašysiu, nes aiškiai netinka mums. Bet paskaitysiu ir pažiūrėsiu dėl duomenų.



Tekstas turi veiklos, kuri mums yra svarbi, bet čia ATLAS duomenys, tai kaip ir anksčiau minėjau, visokiausių atrankos dalykų aš nerašiau.

**[Measurement of the top quark mass using lepton transverse momenta with the ATLAS detector at √s = 8 TeV](http://cds.cern.ch/record/2639902?ln=en)**

Straipsnis yra ilgokas. Pabandysiu atidžiau skaityti, tačiau čia yra ATLAS parametrai, tai nežinau ar labai to reikia. Vis dėlto, čia yra data sets, į kurios yra specifiškai nurodyta, tai šitas straipsnis yra svarbus.

*Data:*

data12 8TeV.periodA.physics Egamma.PhysCont.NTUP COMMON.grp14 v01 p1517 p1562

data12 8TeV.periodB.physics Egamma.PhysCont.NTUP COMMON.grp14 v01 p1278 p1562

data12 8TeV.periodC.physics Egamma.PhysCont.NTUP COMMON.grp14 v01 p1278 p1562

data12 8TeV.periodD.physics Egamma.PhysCont.NTUP COMMON.grp14 v01 p1278 p1562

data12 8TeV.periodE.physics Egamma.PhysCont.NTUP COMMON.grp14 v01 p1278 p1562

data12 8TeV.periodG.physics Egamma.PhysCont.NTUP COMMON.grp14 v01 p1278 p1562

data12 8TeV.periodH.physics Egamma.PhysCont.NTUP COMMON.grp14 v01 p1278 p1562

data12 8TeV.periodI.physics Egamma.PhysCont.NTUP COMMON.grp14 v01 p1562

data12 8TeV.periodJ.physics Egamma.PhysCont.NTUP COMMON.grp14 v01 p1562

data12 8TeV.periodL.physics Egamma.PhysCont.NTUP COMMON.grp14 v01 p1562

data12 8TeV.periodA.physics Muons.PhysCont.NTUP COMMON.grp14 v01 p1517 p1562

data12 8TeV.periodB.physics Muons.PhysCont.NTUP COMMON.grp14 v01 p1278 p1562

data12 8TeV.periodC.physics Muons.PhysCont.NTUP COMMON.grp14 v01 p1278 p1562

data12 8TeV.periodD.physics Muons.PhysCont.NTUP COMMON.grp14 v01 p1278 p1562

data12 8TeV.periodE.physics Muons.PhysCont.NTUP COMMON.grp14 v01 p1278 p1562

data12 8TeV.periodG.physics Muons.PhysCont.NTUP COMMON.grp14 v01 p1278 p1562

data12 8TeV.periodH.physics Muons.PhysCont.NTUP COMMON.grp14 v01 p1278 p1562

data12 8TeV.periodI.physics Muons.PhysCont.NTUP COMMON.grp14 v01 p1562

data12 8TeV.periodJ.physics Muons.PhysCont.NTUP COMMON.grp14 v01 p1562

data12 8TeV.periodL.physics Muons.PhysCont.NTUP COMMON.grp14 v01 p1562


Papildomų šaltinių čia nepridėjau, nes visiškai nemau, kad jie yra reikalingi. Bet pradėjau abejoti, kad nereikia atrankos parametrų iš ATLAS.

**2020/08/07**

**[Measurement of the associated production of a W boson and a charm quark at s√=8 TeV](http://cds.cern.ch/record/2682200?ln=en)**

Kodėl visi straipsnių search engines tokie broken?

*Parametrai:*

integrated luminosity 19,7fb^-1

anti k clustering R=0,5

electron pT>27GeV

muon pT>25GeV

pseudorapidity |eta|<2,1

lepton pT>30GeV (dabar neaišku, kaip jie čia daro, turbūt pirmi parametrai yra veto)

Mt>55GeV

yra dar jets parametrai, bet manau jų nereikia

**[Measurement of the production cross section of a W boson in association with two b jets in pp collisions at s√= 8 TeV](http://cds.cern.ch/record/2210676?ln=en)**

*Parametrai:*

integrated luminosity 19,8 fb^-1

pT>30GeV

pseudorapidity |eta|<2,1 (čia viskas yra duota iš abstrakto, dar perskaitysiu straipsnį, gal dar kažko daugiau bus)

R<0,4

for isolation muon pT>24GeV electron pT>27GeV |eta|<2,1

Mt buvo vietomis nurodyta kaip <20GeV ir <30GeV, tai nelabai aišku, kas ten su jais yra.

Bet atrodo, kad pakankamai daug parametrų yra, nors vis tiek atrodo, kad trūksta kitų parametrų, kaip Emis.

**[Search for massive resonances decaying into pairs of boosted bosons in semi-leptonic final states at sqrt(s) = 8 TeV](https://arxiv.org/abs/1405.3447v2)**

*Parametrai:*

integrated luminosity 19,7 fb^-1

pT for muons>40GeV

minimum transverse energy threshold is 80 GeV for the electrons

The trigger efficiencies for the single-muon trigger vary between 82% and 94% depending on the value of the h of the muon. The efficiency is above 98% for the single-electron trigger.

elektronams (nežinau ar čia jets):

R<0,4

Toliau eina apie elektronų dvigubą skylimą iš Z bozono, kaip ir muonams aš to nerašau.

Čia nėra labai apie parametrus kalbama ir didelį selection apart to, ką aš užrašiau.

**[Measurement of electroweak production of a W boson and two forward jets in proton-proton collisions at sqrt(s) = 8 TeV](https://arxiv.org/abs/1607.06975v2)**

*Parametrai:*

integrated luminosity 19,2 fb^-1

electron pt>30GeV; muon pt>25GeV

electron |eta|<2,5; muon |eta|<2,1

exclusion zone 1,44<|eta|<1,57

electron Emiss>30GeV; muon Emis>25GeV

Mw>30GeV

Daugiau nei nei kitur yra čia parašyta, bet tikiuosi tiek užtenka.

**[Study of vector boson scattering and search for new physics in events with two same-sign leptons and two jets](https://arxiv.org/abs/1410.6315v2)**

Šitame straipsnyje (laiške) yra kalbama apie W bozono skylimą. Tačiau problematiška rasti kalba apie single lepton skylimą. Labiau yra ieškomas same-sign dilepton, kas nėra čia svarbu.

**[Search for anomalous gauge couplings in semi-leptonic decays of WWγ and WZγ in pp collisions at s√= 8 TeV](https://arxiv.org/abs/1310.0473v1)**

*Parametrai:*

Mw>30GeV

elektronui ET>30GeV; |eta|<2,5 exclusion 1,44<|eta|<1,57

muonui pT>25GeV; |eta|<2,1

neutrino ET>35GeV

additional letpon events are vetoed

**[Opportunities and Challenges of Standard Model Production Cross Section Measurements at 8 TeV using CMS Open Data](https://arxiv.org/abs/1907.08197v2)**

Prieš parametrus, aš dar radau iš kur šitas straipsnis ėmė duomenis:

[24] Single electron primary dataset in aod format from run of 2012
(/singleelectron/run2012c-22jan2013-v1/aod). CERN Open Data Portal:
http://opendata.cern.ch. DOI:10.7483/OPENDATA.CMS.BAKP.W6TP.
[25] Single electron primary dataset in aod format from run of 2012
(/singleelectron/run2012b-22jan2013-v1/aod). CERN Open Data Portal:
http://opendata.cern.ch. DOI:10.7483/OPENDATA.CMS.8XN1.J5N7.


Kadangi straipsnis kiek senokas, yra tik su muonu skylimas W bozono

*Parametrai:*

pT>25GeV

|eta|<2,1

ET>25GeV (spėju neutrinams)

mT>40GeV

Nežinau kaip geras šitas straipsnis, nes integrated luminosity yra 1,8 fb^-1

**[A Study of the PDF uncertainty on the LHC W-boson mass measurement](https://arxiv.org/abs/1905.00110v1)**

Kaip suprantu, šie parametrai buvo pritaikyti ir PDFams iš experimental data

*Parametrai:*

ET>20GeV

|eta|<2,5

skirtingi pT ranges for leptons (35 < PlT < 45, 30 < PlT < 50, and 20 < PlT< 60).

boson transverse momentum cuts (PWT < 15 GeV, PWT < 30GeV, PWT < 60 GeV and PWT < 300 GeV)

**[W±Z production at the LHC: fiducial cross sections and distributions in NNLO QC](https://arxiv.org/abs/1703.09065v1)**

Peržiūrėjus straipsnį atidžiau, pasirodo, kad straipsnys nėra reikalingas mūsų tikslams. Jame yra kalba apie šiokius tokius parametrus leptonui, kai W bozonas skyla, tačiau šie parametrai jau buvo anksčiau užrašyti, kaip pavyzdžiui praeitame straipsnyje. Taip pat nėra labai daug parašyta apie tai kur tiksliai yra atranka daroma atskira, tik W bozono skylimui (kaip sakiau, yra duota lentelė su parametrais, bet jie nevisi ir man pasirodė, kad keistokai išrašyti). Bei man tuo pačiu pasirodė, kad duomenys buvo atlikti tik su 13TeV, nors apačioje yra parašyti cross sections at 8Tev. Todėl aš šitą straipsnį atmetu.

**[Measurements of differential cross sections for associated production of a W boson and jets in proton-proton collisions at  s = 8 TeV](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.95.052002)**

*Parametrai:*

integrated luminosity 19.6fb^-1

muon pT>25GeV

|eta|<2.1

cone R<0.4

MT>50GeV

Man tik atrodo, kad gal blogai yra, kad tik muonas, bet gal tiks.

**[Search for supersymmetry in pp collisions at sqrt(s)=7 TeV in events with a single lepton, jets, and missing transverse momentum](https://arxiv.org/abs/1107.1870)**

Iš karto pažymiu, kad čia yra 7TeV, ne 8. Kiek žinau, man reikia ties 8, bet gal dar bus kažkas. Dėl viso pikto, parametrus užsirašysiu. Taip pat, turiu pridėti, kad turėjau panašų straipsnį, kai buvo 8TeV

*Parametrai:*

integrated luminosity 36fb^-1

muons pT>15GeV |eta|<2,4

electrons pT>20GeV |eta|<2,4

ET>25GeV

parametrai yra mažesn nei tų, kurie buvo su 8 TeV.

**[Search for new physics in final states with a lepton and missing transverse energy in pp collisions at the LHC](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.87.072005)**

Šitame straipsnyje nerašoma kaip buvo atrenkami leptonai, bet yra nuoroda į straipsnį, pagal kurį remtąsi, kurio nuoroda yra [čia](https://link.springer.com/article/10.1007/JHEP08(2012)023). Šičia ir vėl eina kalba apie 7 TeV. Šičia parametrai buvo

muons pT>40GeV

electrons pT>85GeV

|eta| <2,1 muons

|eta|<2,5 |eta|>1,56 or 1,442 electrons

Bet aš nežinau ar čia gerai surasti parametrai ar išvis jie yra reikalingi.

**[Search for heavy gauge W′bosons in events with an energetic lepton and large missing transverse momentum at √s=13 TeV](https://www.sciencedirect.com/science/article/pii/S0370269317303179?via%3Dihub)**

Čia yra paieška, kai yra 13TeV, tai nežinau ar tiks.

*Parametrai:*

integrated luminosity 2,3fb^-1

M>4,1TeV

muon:

pT>45GeV

|eta|<2,1

or 

pT>50GeV

|eta|<2,4;

500GeV<Mt<1500GeV

electrons have loose restrictions 

pt>105 or 115 GeV

Dėl elektronų, aš nežinau, čia gali būti kiek kebliau, nes nėra labai aiškiai išrašyta (kaip rašiau, loose restrictions)

**[Search for physics beyond the standard model in final states with a lepton and missing transverse energy in proton-proton collisions at s = 8 TeV](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.91.092005)**

*Parametrai:*

integrated luminosity 19,7fb^-1

muon pT>45GeV

electron Et>100GeV

loose electron restrictions

electron |eta|<1,442 or 1,56<|eta|<2,5

muon |eta|<2,1

electron Mt=2,3TeV

muon Mt=2,1TeV

Tikiuosi, kad radau teisingus parametrus, nes čia buvo daug rašoma apie skirtingus modelius (juodoji materija, sunkusis W' bozonas ir t.t.), bet manau gerai viskas surašyta.

**08/10**

Pradedamas duomenų rinkimas. Čia surašysiu visus duomenų linkus ir jų pvadinimus:

**[SingleElectron primary dataset in AOD format from Run of 2012](http://opendata.cern.ch/record/6046)**

>**[SingleElectron primary dataset in AOD format from RunA of 2011](http://opendata.cern.ch/record/31)**

**[SingleMu primary dataset in AOD format from Run of 2012](http://opendata.cern.ch/record/6047)**

**[SingleElectron primary dataset in AOD format from Run of 2012](http://opendata.cern.ch/record/6020)**

>**[SingleMu primary dataset in AOD format from RunA of 2011](http://opendata.cern.ch/record/32)**

**[SingleMu primary dataset in AOD format from Run of 2012](http://opendata.cern.ch/record/6021)**

> Turint data sets (rastus per [CMS data](http://opendata.cern.ch/search?page=1&size=20&subtype=Collision&type=Dataset&experiment=CMS&file_type=aod#)), galiu pabandyti pažiūrėti į ankstesnius straipsnius, kur yra dar duomenų sets.

Patikrinus, ką turėjau, tai vieni šaltiniai yra iš ATLAS, tai nelabai reikia. O kitą duomenų set turiu jau užrašęs šituose linkuose.

**08/11**

Dabar prasideda visas darbas. Reikia patikrinti duomenis (2011 gali būti 7TeV, tai netinka). Taip pat, prasideda leptono analyzer paieškos.

Patikrinus, visi 2011 straipsniai yra su 7TeV, tai jie netinka. Atskyriau juos pastumdamas.

Reikia pradėti ieškoti analyzerio. Reikalingas single lepton analyzer.

Google per single lepton analyzer paiešką išmėtė vienintelį [analyzerį](https://github.com/emanueleusai/singleLepAnalyzer). Nežinau ar jis tinka, man atrodo jis su jets tik dirba. Paieškosiu dar.

Radau dar per githubą su single lepton raktažodžiais [vieną](https://github.com/eliasron/singleLepton), bet nežinau ar jis geras, README mažai parašyta. 

Iškilo problema, kad negaliu git clonint į virtualią mašiną. Bandžiau ieškoti klaidos. Yra kalbos apie portų keitimus (neradau, kaip virtualioje mašinoje tai pakeisti). Yra kalbos apie http ir https susiekimą, tačiau neradau jokių sutvarkimų iš tos pusės, nes aš kreipiuosi į https (bandžiau ir į http, bet vis tiek ta pati klaida). Turbūt portai klaida, bet reikės pažiūrėti dar sprendimų.

Radau, kaip ištaisyti *cloninimo* klaidą. Prieš terminale klonavimą reikia parašyti git config --global --add http.sslVersion tlsv1.2 (klaida buvo server_hello klaida, tai čia dėl https ir http).

**08/12**

Per githubą paieškojau kodų, kurie galėtų atrinkti missing energy. Su raktiniaisi žodžiais missing energy radau [vieną](https://github.com/vergili/MonoJet/tree/master/NtupleAnalyzer/python), kuris asocijuojasi su mano darbu. Gal bus geras.

Bandžiau dar paieškoti kodų su tais pačiais raktiniais žodžiais, bet neradau. Pakeitus į missing transver energy išmetė du rezultatus. Vienas buvo kodas aukščiau nurodytas. Antras buvo tex kodas, kas netinka. Kolkas nenagrinėsiu labai jau to kodo, ryt turbūt pradėsiu.

**08/13**

Iš dalies susitvarkiau vu prisijungimus, tačiau aš galiu tik prie vu.is.lt prisijungti ir prie nieko kito. Gal čia reikia palaukti kiek laiko, kol galėsiu, vėliau pridėsiu.

**08/17**

Pradėjau ieškoti kodų, reikalingų pridėti Emiss. Iš tiek, kiek skaičiau twiki.cern.ch ir iš kitų kodo (specifiškai Donato Liupsevičiaus), man atrodo atradau, kaip pridėti Emiss. Reikia pridėti eilutę self.electronHandle=Handle('std::vector<pat::Electron>'), tik Electron pakeisti į MET. Tada reikia tik pridėti sąlygas, reikalingas atmesti Emiss nereikalingus atvejus ir šiaip patikrinimą. Tada reikės pereiti per Two lepton analyzer ir pridėti atrinkimus Emiss ten (bei sutvarkyti atrinkimo parametrus leptonams). Kadangi aš su pythonu nesame geriausi draugai, tai bus kiek sudėtinga, bet vis tiek pabandysiu. Taip pat, pridedu dar senesnį žurnalą, kur rašiau apie straipsnius ir jų paieškas. Liko tik pridėti perdarytą root tutorialą ir viskas turėtų suveikti.

**08/18**

Pridėjau reikiamas eilutes prie Analyzer.py, kad jis nuskaitytų MET. Ar jis tai darys, tai neaišku, reikės patikrinti. Bet bandydamas primesti naują failą su git komandomis, man pradėjo mesti error'ą "[rejected] branch-name->branch-name (not-fast-forward). Sutvarkyti šitą problemą reikėjo parašant eilutę git pull origin branch-name ir tik tada visus git push daryti.

**08/19**

Taigi, pradėjau kodo tikrinimą ir sėkmingai kodas suveikė (kai iš pradžių paleidau, pamiršau, kad neparašiau terminale cmsenv, kas leidžia kodui veikti). Tačiau yra dvi problemos. Aš nemačiau E miss duomenų, tai negaliu patikrinti ar juos išvis normaliai paėmė. Taip pat, kadangi kodas nesaugo duomenų, tai negaliu ir taip patikrinti ar yra kažkas (jis tik brėžia grafiką.). Taigi, reikia rasti, kaip saugoti tokius duomenis, juos apdoroti ir išgauti kažką naudingo. Su tuo manau, jau čia C++ reiktų, bet kadangi aš dar nelabai žinau savo krypties, tai dar kolkas neaišku.

**08/21**

Šiandien patikrinius kitus kodus, kurie galėtų veikti geriau, nei Donato kodas. Kolkas nedėsiu visų savo pinigų į C++ ir pradėsiu labiau su pythonu aiškintis. Iš tiek, kiek skaičiau pavyzdžius ar twiki, tai aš kiek ir suprantu, kaip nuskaityti MET, bet išsaugoti nelabai žinau. Gal pavyzdžiai padės.

Taigi, parsisiųstas kodas buvo nelabai aiškus, vėliau bandysiu clonintis twiki direktorijas (apie tai vėliau aprašyta), gal ten bus aiškiau.

**08/24**

Kadangi reikia toliau judėti prie darbų, pradėjau PAT tuple failų gamybos tutorialą daryti, kurį pasiūlė Marijus. Persiskaičius tutorialą viskas buvo aišku ir prisėdau išbandyti jį. Tačiau problema iškilo, kai bandžiau gittinti į direktoriją. Reikėjo github account'o. Jį pasidarius man pasakė, kad neturiu teisingų access rights ir negaliu nuklonuoti pirmos direktorijos. Su tuo baigiu tutorialo tikrinimą ir bandau ieškotis sprendimų.

**09/08**

Kolkas, kiek nedetaliai bus aprašyta, nes nelabai turiu failų ir nuorodų, kurias galiu dalintis. Kolkas atlikinėjau testus su C++ kodo pakeitimais, kad atrinktų prarastą energiją (missing energy arba MET). Viską dariau, pagal[Marijaus padarytą tutorialą](https://gitlab.com/marijusambrozas/cms-open-data/-/blob/master/PATtuple_analize.md).

Viskas prasidėjo kiek anksčiau (apie 08/22), kai CMS Shell terminale aš įrašiau dvi eilutes 
```
git cms-addpkg PhysicsTools/PatAlgos
git cms-addpkg PhysicsTools/PatExamples
```
Bet su tuo prasidėjo problema. Negalėjau klonuotis direktorijų. Problema buvo tame, kad aš neturėjau github'o paskyros (gitlab'o neužtenka). Todėl susikūręs paskyrą galėjau nusiklonuoti direktorijas ir pradėti toliau mokymąsį. Manau, kad labai aprašynėti patį tutorialą nereiktų antrą kartą, bet reikiamas vietas aš pažymėsiu ir kur buvo klaidos man.

Taigi, nusiklonavus minėtas direktorijas, buvo pradėtas tikrinimas ar veikia jos. Tam, kad jos mums atitiktų, kaip nurodyta tutoriale buvo padaryti pakeitimai. Taip pat, kad ir nebūtina, bet buvo pridėta eilutė pythono faile patTuple_standard_cfg.py

`process.selectedPatMuons.cut = 'pt > 20. && abs(eta) < 2.4'`

Aišku, ne viskas iš karto suveikė. Buvo klaidų. Bet jos buvo perrašymo klaidos, kadangi pas mane kažkodėl negalima į virtual machine kopijuoti kodo (bandau dar ieškoti sprendimo į šitą problemą, bet nėra prioritetas man), todėl atsirado tokios klaidos.
Kodas ilgai veikė (kaip pirmam kartui irgi ilgai, lėtas kompiuteris), bet galų gale suveikė ir aš perėjau prie darbo su PAT Tuple failų analize (prieš tai buvo PAT Tuple gamyba.)

Darbas buvo vis tiek su Marijaus tutorialu, todėl taip pat nėra labai reikalo čia jo perrašinėti viso. Bet prieš tai, aš patTuple_standard_cfg.py failo eilutėje 

`process.maxEvents.input = 100`

skaičių `100` pakeičiau į `10000`, kas man leido pamatyti, kad gaunami grafikai yra lygesni, bet vis dar tokie kampuoti (nelabai kaip dabar turiu grafikus atsisiųsti ir parodyti, kaip pavyzdį, tai teks mano žodžiu pasitikėti). Tačiau vėliau (tiksliau 09/07 dieną) sužinojau, kad nelabai reikia to daryti, nes pakeitus skaičių į `-1` bus pereinama per visus įvykius, kas sutvarkys nelygumus.

Su tuo, tęsiau darbą ir pradėjau darbą su C++ kodais, tiksliau su PatMuonAnalyzer.h ir PatMuonAnalyzer.cc. Su jais iškylo keblesnės bėdos, kurios buvo segmentation errors. Bandžiau naršyti per google su raktiniais žodžiais segmentation error, bet galų gale su Marijaus pagalba buvo suprasta, kad tai buvo perrašymo klaida. Ir tada buvo išanalizuotas PAT Tuple failas `patTuple.root`, kuriame buvo palikti tik grafikai su muono ir MET parametrais (pT, phi ir delta(man atrodo, reiktų dar patikrinti)).

Tada buvo daroma trečioji sekcija, kur reikėjo pagaminti kitą root failą. Ten reikėjo  veikti taip pat su paminėtais failais, bet pridėti atitinkamas eilutes, kaip buvo nurodyta tutoriale. Tačiau šioje situacijoje ir vėl pasirodė segmentation errors. Iš pradžių, maniau, kad gal kur klaida buvo perrašyme, kaip praeitą kartą. Tačiau net ir viską vėl iš naujo peržiūrėjus nebuvo rasta jokios tokios klaidos. Tad vėl su Marijaus pagalba buvo peržiūrėta ir buvo rasta klaida. Faile PatMuonAnalyzer.h  yra eilutės 
```
/// everything that needs to be done before the event loop
void beginJob(){};
```
ir 
```
/// everything that needs to be done after the event loop
void endJob(){};
```
Jos turėjo būti atitinkami aprašytos
```
/// everything that needs to be done before the event loop
void beginJob(){
f_out = new TFile("OUTFILE.root", "RECREATE"); // Creating the file
OUTFILE.root
f_out->cd();
t_out = new TTree("TREE", "TREE"); // Creating a tree with name TREE
t_out->Branch("muon_pT", &muon_pT); // One branch of the tree will be muon_pT
t_out->Branch("MET_pT", &MET_pT); // Another branch will be MET_pT
};
```
ir 
```
/// everything that needs to be done after the event loop
void endJob(){
f_out->cd(); // to make sure we are writing into this file
t_out->Write();`
f_out->Close();
};
```
Bet aš padariau klaidą ir šitą kodą užrašiau į private sektorių 
```
private:
/// input tag for mouns
edm::InputTag muons_;
edm::InputTag METs_;
/// histograms
std::map<std::string, TH1*> hists_;
// Output file and tree
TFile *f_out;
TTree *t_out;
// Custom defined event variables
std::vector<double> *muon_pT;
double MET_pT;
void beginJob(){
f_out = new TFile("OUTFILE.root", "RECREATE"); // Creating the file
OUTFILE.root
f_out->cd();
t_out = new TTree("TREE", "TREE"); // Creating a tree with name TREE
t_out->Branch("muon_pT", &muon_pT); // One branch of the tree will be muon_pT
t_out->Branch("MET_pT", &MET_pT); // Another branch will be MET_pT
};
`void endJob(){
f_out->cd(); // to make sure we are writing into this file
t_out->Write();
f_out->Close();
};
```
kas ir metė problemą, nes turėjo viską prasidėti prieš visus ciklus ir deklaracijos tada nieko nereiškė. Sutvarkytas kodas leido pilnai ir sėkmingai užbaigti tutorialą.

Po šito pradėta pridėdinėti elektrono parametrų atrinikimą. Kad galėtume atrinkinėti elektronus, mus reikia juos atpažinti. Tam reikia pridėti eilutes kodo PatMuonAnalyzer.cc pradžioje pridėti eilutes
```
#include "DataFormats/EgammaCandidates/interface/GsfElectron.h
#include "DataFormats/PatCandidates/interface/Electron.h
```
Bet čia susidūriau su problema, kad nežinojau koks yra elektronų handle, t.y. kaip pakeisti kodo eilutes 
```
edm::Handle<std::vector<Muon> > muons;
event.getByLabel(muons_, muons);
```

kad atitiktų elektronui. Aš bandžiau Handle dalyje keisti į Electron, Electrons, Elec, ElectronCollection, bet vis tiek metė klaidą, kad nėra tokio handle. Šituos handles aš bandžiau atspėti iš (DataFormats, kur buvo parašytos visos saugyklos)[https://github.com/cms-sw/cmssw/tree/CMSSW_5_3_X/DataFormats]. Tačiau man nepavyko rasti sėkmingo Handle.

**09/09**

Iš pradžių pradėjau nuo vakarykščio darbo, kur reikėjo pridėti elektroną. Pasirodo buvau nepridėjęs keletos eilučių PatMuonAnalyzer.h ir PatMuonAnalyzer.cc, kurios buvo paprastos eilutės, nukopijuotos iš muonų dalies, kad atitiktų elektronus:
```
edm::InputTag electrons_
std::vector<double> *electron_pT (čia abi yra PatMuonAnalyzer.h faile, ten, kur yra parašytos tos pačios eilutės apie miuonus)
```
```
electrons_(cfg.getParameter<edm::InputTag<("electrons"))
using pat::Electron;
edm::Handle<std::vector<Electron> > electrons
event.getByLabel(electrons_, electrons);
for (std::vector<Electron>:: const_iterator elec=electron->begin(); elec1!=electron->end(); ++elec1){
 hists_["electronpT"]->Fill( elec1->pt  () );
} (čia visos eilutės yra PatMuonAnalyzer.cc faile, kaip ir PatMuonAnalyzer.h, kur parašyta tas pats apie miuonus (ne pačiuose cikluose))
```

Problemos iškilo kompiliavime, metė klaidas, kad nebuvo tokio Handle ir nebuvo taip definuoti dalykai. Tai reikėjo pakeisti dvi eilutes PatMuonAnalyzer.cc faile (Handle buvo parinktas, kad atitiktų miuonų Handle, nes kitokio nebuvo (tai nustačiau pasikonsultavęs su Marijumi))
```
electrons_(cfg.getParameter<edm::InputTag>("electrons"))
for (std::vector<Electron>:: const_iterator elec=electrons->begin(); elec1!=electrons->end(); ++elec1){
```

Nuo tada kodas visas susikompiliavo, bet neveikė. Atsirado ir vėl segmentation error. Po ilgesnės paieškos, buvo atrasta klaida susijusi su eilute 

`muon_pT->clear();` (PatMuonAnalyzer.cc faile)

Kad ištaisyti šią klaidą, faile PatMuonAnalyzer.h reikėjo pridėti eilutę 

`muon_pT = new std::vector<double>` (pradžioje beginJob funkcijos)

Su tuo, kodas suveikė ir sukompiliavo viską. Kadangi tai galima buvo padaryti, aš pridėjau elektrono parametrus, kad atrinktų bei, kad elektrono pT sudėtų į atskirą root failą, kaip ir su muonais ir METais.

**09/14**

Pradėjau nuo kodų pridėjimo prie šitos saugyklos. Bet neišėjo pridėti iš pat pradžių. Kai norėjau pridėti (`git add` komanda) kodus, tai man metė klaida
```
!rejected  master -> master
it appears you don't have the files locally and/or another repository is trying to access them` (ar kažkas tokio, nebepamenu visos klaidos bei nebegaliu jos atrasti.)
```

Taip pat yra kalbama apie tai, kad reiktų apjungti saugyklas ir `git pull` daryti.

Kadangi nelabai norėjau daryti `git pull`, nes jis ten irgi metė visokius įdomius dalykus, kaip prašymus pakomentuoti, kodėl aš noriu apjungti saugyklas, pabandžiau kitaip padaryti. Aš ištryniau virtualioje mašinoje esančią saugyklą ir padariau `git clone git@gitlab.com:aurvit/cmsdataav` ir taip vėl gavau saugyklą, į kurią įkėliau kodus ir vėl paleidau `git add` komandą. Šį kartą suveikė. Bet pamiršau pakeisti folderių pavadinimus, kad jie būtų aiškesni. Todėl pakeitęs pabandžiau vėl `git add`, bet dabar saugykloje (bent internetinėje) turiu keturis folderius, o ne du, kurie vieni angliškai pavadinti, kiti lietuviškai. Paieškosiu kaip nors, kaip sutvarkyti šitą problemą.

Tuomet pradėjau pridėti dar keletą parametrų į atrinkimą iš root failo. Reikėjo pridėti elektronamas, miuonams po krūvio parametrą ir MET'ams phi parametrą. Panaršęs po sudarytą root failą aš atradau objektą su pabaiga `.charge_` tai pagal tai sudariau atrinkimą

```
hists_["muonCharge"]->Fill( mu1->charge() );
```
Taip pat pritaikiau tai elektronui. O MET'ams pridėjau atrinkimą
```
hists_["METPhi"]->Fill( met1->phi() );
```
Taip pat reiktų pridėti, kad šitie atrinkimai vyksta for cikluose. Prieš juos mes pridedame dar eilutes, kur sukuriame grafikus, tačiau jos bus panašios į praeitas jau aprašytas eilutes, tik pasikeis tam tikri žodžiai į Charge ar charge bei MET'ai gaus phi histogramą.
Patikrinus kodo veikimą, yra padaromi atrinkimai. Su MET man atrodo yra viskas gerai, atrodo, kaip ir elektronų ar miuonų grafikai. Bet su elektronų ir miuonų krūviais man kiek nepatinka. Kol negaliu parodyti grafikų, galiu tik apibūdinti, kad labai aštrūs pykai ir eina į neigiamas vertes (x ašyje, ne y). Bet gal tai yra normalus dalykas.
Bandžiau paprastai pakeitimus perkelti į saugyklą su `git add`, bet man vėl metė tą pačią klaidą, kaip anksčiau. Kol kas negaliu parodyti klaidos, nes man kažkodėl bidirectional copy-paste nesuveikia. Todėl man teko perrašyti eilutes papildomas į saugykloje esantį PatMuonAnalyzer.cc. Galvoju kaip geriau padaryti, nes nesinori vis trinti ir klonuoti, bei gauti papildomų direktorijų.

**09/15**

Pridėjus visus parametrus, kurie yra reikalingi, bent kiek žinau, praeitiems rinkiniams, pridėjau PFJets. Kad juos pridėti, reikia PatMuonAnalyzer.cc prirašyti
```
#include "DataFormats/JetReco/interface/PFJet.h
#include "DataFormats/PatCandidates/interface/Jet.h
```
Ir tada visur (PatMuonAnalyzer.h ir PatMuonAnalyzer.cc failuose), kaip su kitais rinkiniais užrašome kodo eilutes, tik pakeičiame ten, kur reikia į jet ar jets.
Sutvarkius tai, viskas susikompiliavo. Tačiau paleidus analyzePatMuons_edm_cfg.py programa nesuveikė, neatpažino kintamojo "jets". Tam sutaisyti, šitame python'o faile eilutėje
```
process.patMuonAnalyzer = cms.EDAnalyzer("PatMuonEdAnalayzer,
muons = cms.InputTag("cleanPatMuons"), METs = cms.InputTag("patMETs"), electrons = cms.InputTag("cleanPatElectrons"),
)
```
reikia pridėti eilutę `jets = cms.InputTag("cleanPatJets")`, kad atrodytų taip
```
process.patMuonAnalyzer = cms.EDAnalyzer("PatMuonEdAnalayzer,
muons = cms.InputTag("cleanPatMuons"), METs = cms.InputTag("patMETs"), electrons = cms.InputTag("cleanPatElectrons"), jets = cms.InputTag("cleanPatJets"),
)
```
Tai padarius, kodas suveikia ir gauname reikalingus dalykus.

**09/18**

Padarius, kad pridėtų Jets, buvo pereita prie papildomų atrinkimo parametrų. Šie atrinkimo parametrai yra bDiscriminator ir partonFlavour. partonFlavour, gana lengva yra pridėti. Kaip su visais parametras, tiesiog pakeiti kokį parametrą į `partonFlavour` ir viskas.
Didesnės bėdos atsirado su bDiscriminator, nes iš pradžių grafikas nieko nerodė. Patikrinus vertes, visos vertės buvo -1000. Reiškia kažkur buvo klaida. CMSSW_5_3_X DataFormats, ieškojau bDiscriminator panaudojimo ir kaip jį geriau panaudoti. Kurį laiką aš nieko neradau, bet tai buvo tik todėl, kad aš žiūrėjau .h failus, o ne .cc, kur yra detaliau aprašyta bDiscriminator. Iš .cc failų bei iš [cern wiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookBTagging) buvo nustatyti galimi tag'ai, kad nemestų klaidos. Patikrinus visus tag'us, buvo pasirinktas `trackCountingHighPurBJetTags` tag'as. Prirašymas turėjo būti toks

```
hists_["jetbDiscriminator1"]->Fill( jt1->bDiscriminator("trackCountingHighPurBJetTags") );
```
čia jetbDiscriminator1 yra pavadintas taip, nes tai tik relikvija iš visų tag'ų testavimo. Pasirinkimas buvo padarytas pagal gautus grafikus ir kaip jie atrodo (tik 4 grafikai iš 9 buvo ne tušti ir tik 2 iš jų buvo panašūs. Grafikas su tag'u trackCountingHighEffBJetTags buvo atmestas, nes jo vertės buvo didesnės).

Sutvarkius tai ir dar nežinant ar reikia *isolation*, buvo pradėta pridedinėti šituos naujus paramterus prie kuriamo root failo. Tai buvo nesudėtingai padaryta, tik reikėjo prirašyti papildomas eilutes, kur jau buvo pridėti elektrono ir miuono parametrai, bet pakeičiant jas į Jets ir juos atitinkančius parametrus. Tokie pakeitimai buvo padaryti PatMuonAnalyzer.h ir PatMuonAnalyzer.cc failuose. Tai pavyko padaryti be jokių klaidų ir buvo pereita prie kitų darbų.

Tada, pagal Marijaus nurodymus, buvo pradėta aiškintis, kaip geriau suorganizuoti, sutvarkyti kuriamą root failą. Skaičiausi puslapį, kuriame buvo kalbama apie [root medžius ir šakas](https://root.cern.ch/doc/master/classTTree.html). Tačiau iki pat galo nelabai supratau, kaip padaryti atskiras šakas. Ir tik po to sužinojau iš Marijaus, kad vis dėl to nereikia daryti atskirai, svarbu tik sudėti viską į root failą ir to užteks.

Su tuo, sužinojau apie *isolation*. Miuonui pridėjau 4 isolation eilutes 
```
hists_["muonsumChargedHadronPt"]->Fill( mu1->pfIsolationR04().sumChargedHadronPt );
hists_["muonsumNeutralHadronEt"]->Fill( mu1->pfIsolationR04().sumNeutraulHadronEt );
hists_["muonsumPhotonEt"]->Fill( mu1->pfIsolationR04().sumPhotonEt );
hists_["muonsumPUPt"]->Fill( mu1->pfIsolationR04().sumPUPt );
```

faile PatMuonAnalyzer.cc for cikle. Apart klaidelių, kur blogai perrašiau kažką viskas suveikė ir išėjo ištraukti šituos parametrus. Tada pridėjau panašius parametrus elektronui

```
hists_["electronsumChargedHadronPt"]->Fill( elec1->pfIsolationVariables().sumChargedHadronPt );
hists_["electronsumNeutralHadronEt"]->Fill( elec1->pfIsolationVariables().sumNeutraulHadronEt );
hists_["electronsumPhotonEt"]->Fill( elec1->pfIsolationVariables().sumPhotonEt );
hists_["electronsumPUPt"]->Fill( elec1->pfIsolationVariables().sumPUPt );
```
tačiau atsirado klaida. Nesusikompiliavo ir metė klaidą
```
PflowIsolationVariables has no member "bet kurio iš nurodytų parametrų pavadinimas"
```
kas yra kiek keistoka, nes aš nerašiau `PflowIsolationVariables`. Pabandęs pakaitalioti užrašymą, vis tiek niekas nepasikeitė ir meta klaidą.

Visas šitas aprašymas buvo darbų nuo 09/16 iki 09/18.