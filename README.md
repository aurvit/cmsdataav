Kolkas tai tegu bus zurnalas

2020/08/03:
Pradedama tikrinti straipsnius.
Pirmas straipsnis
Study of the Higgs boson produced in association with a weak boson and decaying to WW* with a same sign dilepton and jets final state in sqrt(s)=8TeV PP collisions with the ATLAS detector at the LHC

216 puslapiu. Pabandysiu kuo lengviau ir greiciau perskaityti, bet bijau, kad uztruksiu.
Kolkas per didelis straipsnis, pabandysiu veliau

Antras straipsnis
New results on W boson production with the ATLAS detector

Straipsnis yra 4 puslapiu, zymiai trumpesnis.
Straipsnyje nera pazymeta, kur yra kokie jets, bet kalba eina apie koregavima W bozonui rasti, tai atrodo, kad gali tikti.

Trecias straipsnis
New Results on W Boson production and multi-lepton cross sections with the ATLAS detector

Straipsnis net ne straipsnis, o powerpoint presentation. Bet gal bus usefull.
Kalba eina apie muonu produkcija, ne elektronu, kas nera reikalinga.

Ketvirtas straipsnis
New results on W boson production with the ATLAS detector

Straipsnis ir vel powerpoint.
Vis del to sitame straipsnyje buvo daugiau apie jets, taciau duomenu susijusiu su jais nelabai buvo.

Penktas straipsnis
Measurment of the top quark mass using lepton transverse momenta with the ATLAS detector sqrt(s)=8TeV

115 puslapiu. Gal ir vel paliksiu velesniam laikui.
Sitame straipsnyje yra nuorods i data lists, kas yra labai gerai. Tik dar reikes mismokti apdoroti tuos duomenis reikiamai. Taip pat reikia pazymeti, kad sitame straipsnyje yra ieskoma top kvarko mase, kas gali kaip nors prisideti prie duomenu analyzes.

Sestas straipsnis
Combination of the W boson polarization measurments in top quark decays using ATLAS and CMS data at 8 TeV

Straipsnis vel powerpoint.
Sitas kiek keblesnis. Jis matuoja W bozono ir vieno leptono atsiradima skilus top kvarkui. Yra pazymeta, kad yra W bozonas ir dileptonas (straipsnio nenaudojamas) tai tada kyla klausimas ar sitas vertas kazko straipsnis. Kolkas priimsiu, kad verytas.

Septintas straipsnis pasirodo buvo straipsnis, pagal kuri buvo padarytas sestas straipsnis. Ten duomenys nerasti.

Astuntas straipsnis
Measurement of the associated production of a W boson and a charm quartk at sqrt(s)=8TeV

Mazai tikimybes, kad cia bus kalba apie  dileptonus, bet gal bus.
Tikimybe buvo maza, bet nepasiteisino ji. Matuojamas W bozonas per elektrona (muona) ir neutrina, ko mums nereikia.

Devintas straipsnis

Measurement of single top-quark production in association with a W boson in the single-lepton channel at sqrt(s)=8TeV with the ATLAS detector

Nusimato, kad tikrai nebus cia nieko.
Sitas kiek keistas. Jis kalba apie skylima leptonini ir atrodo, kad gali buti tikrinamas W bozonas pagal dileptonini skylima. Bet tuo paciu nelabai yra tam duomenu, tik atrinkimas elektronu.

Desimtas straipsnis
Measurement of the production cross section of a W boson in association with two b jets in pp collisions at sqrt(s)=8TeV

b jets man atrodo netiks.
Kaip ir sakiau, netinka, nes W bozonas atrenkamas pagal single lepton and neutrino.

Vienuoliktas straipsnis
Searches for supersymmetry based on events with b jets and four W bosons in pp collisions at 8TeV

Atrodo labiau promising, nes atrodo, kad apie visus channels kalbes
Yra kalba apie dilepton tikrinima, kuris yra menkas. Yra references i dilpetons. Gali buti labai useful.

Dvyliktas straipsnis
Measurement of the W boson helicity in events with a signle reconstructed top quark in pp collisions at sqrt(s)=8TeV

cia W bozonas skyla i viena leptona ir neutrino. Tai nera svarbu

Tryliktas straipsnis
Search for massive resonances decaying into pairs of boosted bosons in semi-leptonic final states at sqrt(s)=8TeV

Gaila, bet sitame straipsnyje W bozonas tikrinamas pagal skylima i elektrona ir neutrina.

Straipsniai buvo rasti pasitelkus raktiniais zodziais W boson, jets, pp, 8 TeV. Po to juos atrinkau paskaitydamas abstraktus ir ieskodamas, kur tiksliau kalba apie W bozono skylima ir detection. Liko dar vienas didelis straipsnis, kuri dabar perziuresiu.

Pirmas straipsnis (is naujo)

Del laiko stokos as neskaitysiu viso straipsnio, labiau stengsiuosi atrasti, kur yra kalba apie W bozono detection. Is abstrakto as pastebiu, kad kalba eina apie same charge leptons, kas kelia vilciu, kad sitame straipsnyje reiktu gyliau pasikapstyti.
Na sunku pasakyti ar tai yra geras straipsnis. Yra matuojami channels dilepton, bet taip pat sunku pasakyti, kad ten viskas ko reikia, nes ten techniskai kalba eina apie Higso bozono skylima ir W bozonus ir tada ju skylima i leptonus (ir du kvarkus), kas nezinau kiek yra tai ko, ieskau. Bet straipsnis tuo paciu atrodo svarbus ir ji labiau isstudijavus butu gal geriau nusakyta jo svarba, bet bijau, kad as nesu tiek gerai su visa metodologija susipazines, kad galeciau pilnai suprasti, kas ten parasyta.

2020/08/04:

Trecias straipsnis (is naujo)

powerpointas, kuris man atrodo remiasi duomenimis is pirmo straipsnio. Manau, kad verta perziureti, nes yra kalbama apie skilimus, kurie reikalingi.

Astuntas straipsnis (is naujo)

Perziurejus atrinkinejamas W bozonas per lV skylima, kas yra reikalinga.

Devintas straipsnis (is naujo)

Kalba eina apie skylima i lV tai manau tinka

Dvyliktas straipsnis (is naujo)

Nelabai zinau ar jis tinka. Vienoje vietoje, apie atranka autorius kalba, kad neutrinas nedetectinamas todel ten imbalance atsiranda, taciau abstrakte raso apie single lepton ir two jets. Del sito nelabai zinau.

Tryliktas straipsnis (is naujo)

Yra kalba apie reikiama skylima, manau tinka

Keturioliktas straipsnis
Measurement of electroweak production of a W boson and two forward jets in proton-proton collisions at sqrt(s)=8TeV

Nusimato, kad gal bus, ko reikia
Gali buti naudingas

Penkioliktas straipsnis
Search for heavy Majorana neutrinos in e+/-e+- plus jets and e+/-mu+/- plus jets events in proton-proton collisions at sqrt(s)=8TeV

Eina kalba apie Majorana neutrino skylima i W bozona, bei energy range yra geras. Taciau, kad ir yra reikiamu dalyku, cia W bozonas skyla i qq, tai nesvarbu.

Sesioliktas straipsnis
Improving Constrains on Proton Structure using CMS measurements

Gal bus kazko naudingo.
Nors pasakyta, kad yra range 7 ir 8 TeV, vis del to yra tik 7 ir nelabai parasyta apie W bozona ir jo duomenis

Septynioliktas straipsnis
Search for a massive resonance decaying into a Higgs boson and aW or Z boson in hadronic final states in proton-proton collisions at sqrt(s)=8TeV

Stebes W bozona, gal bus apie ji daugiau kalbama



Vis delto netinka, nes stebimas W bozonas skyla i qq

Astuonioliktas straipsnis
Study of vector boson scattering and search for new physics in events with tow same sign leptons and two jets

Gal bus kazkas reikalingo
Atrodo, kad gal yra kazko svarbaus, bet tuo paciu labai keistai parasytas straipsnis, bet gal yra is to naudos.

Devynioliktas straipsnis
Search for anomalous gauge couplings in semi-leptonic decays of WWgamma and WZgamma in pp collisions at sqrt(s)=8TeV

Neradau is abstrakto, kaip stebi W bozona, bet gal bus kas nors gero.
Atrodo, kad yra kalbos apie reikiama skylima, tai gal tiks.

Dvidesimtas straipsnis
Opportunities and Challenges of Standard Model production cross section measurements at 8 TeV using CMS Open Data

Gal bus kazkas
Atrodo, kad yra kazkas susije su W bozonu ir reikiamu skylimu.

Dvidesimt pirmas straipsnis
A study of the PDF uncertainty on the LHC W-boson mass measurement

Susijes su W bozono mases matavimais, gal tiks
Matavimai parodyti tik 7TeV srityje, bet gale pasako, kad 8 ir 13 TeV srityse gaunami panasus rezultatai, gal visgi tiks.

Dvidesimt antras straipsnis
Combination of searches for heavy resonances decaying to WW, WZ,ZZ, WH, and ZH boson pairs in proton-proton collisions at sqrt(s)=8 and 13TeV

Nezinau, kaip gerai cia, bet stebejo W bozonus, tai gal bus gerai.
Vis delto netinka, nes stebejimas yra W i qq

WplusminusZ production at the LHC: fiducial cross sections and distributions in NNLO QCD

Is abstrakto matosi, kad kalba eisi apie leptonini skylima

