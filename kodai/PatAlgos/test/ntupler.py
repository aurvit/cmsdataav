## import skeleton process
from PhysicsTools.PatAlgos.patTemplate_cfg import *

from PhysicsTools.PatAlgos.tools.coreTools import *
removeMCMatching(process, ['All'])

## add trigger information to the configuration
from PhysicsTools.PatAlgos.tools.trigTools import *
switchOnTrigger(process)

## ------------------------------------------------------
#  NOTE: you can use a bunch of core tools of PAT to
#  taylor your PAT configuration; for a few examples
#  uncomment the lines below
## ------------------------------------------------------
#from PhysicsTools.PatAlgos.tools.coreTools import *

## remove MC matching from the default sequence
# removeMCMatching(process, ['Muons'])
# runOnData(process)

## remove certain objects from the default sequence
# removeAllPATObjectsBut(process, ['Muons'])
# removeSpecificPATObjects(process, ['Electrons', 'Muons', 'Taus'])

## ------------------------------------------------------
#  In addition you usually want to change the following
#  parameters:
## ------------------------------------------------------
#
#   process.GlobalTag.globaltag =  ...    ##  (according to https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideFrontierConditions)
#                                         ##

# globaltag
process.GlobalTag.connect = cms.string('sqlite_file:/cvmfs/cms-opendata-conddb.cern.ch/FT_53_LV5_AN1.db')
process.GlobalTag.globaltag = 'FT_53_LV5_AN1::All'

# DATA
#
# TTBAR
#from PhysicsTools.PatAlgos.patInputFiles_cff import filesRelValProdTTbarAODSIM
#process.source.fileNames = filesRelValProdTTbarAODSIM

# SingleMuon Data
#luminosity
import FWCore.ParameterSet.Config as cms
import FWCore.PythonUtilities.LumiList as LumiList
myLumis = LumiList.LumiList(filename='Cert_190456-208686_8TeV_22Jan2013ReReco_Collisions12_JSON.txt').getCMSSWString().split(',')
process.source.lumisToProcess = cms.untracked.VLuminosityBlockRange()
process.source.lumisToProcess.extend(myLumis)


#input file

import FWCore.Utilities.FileUtils as FileUtils
files2012data = FileUtils.loadListFromFile('CMS_Run2012C_SingleMu_AOD_22Jan2013-v1_20000_file_index.txt')
readFiles = cms.untracked.vstring( * files2012data )
process.source.fileNames = readFiles


#                                         ##
process.maxEvents.input = 100
#                                         ##
#   process.out.outputCommands = [ ... ]  ##  (e.g. taken from PhysicsTools/PatAlgos/python/patEventContent_cff.py)
#                                         ##
process.out.fileName = '~/pattuples/patTuple1.root'
#                                         ##
process.options.wantSummary = False   ##  (to suppress the long output at the end of the job)

process.selectedPatMuons.cut = 'pt > 20. && abs(eta) < 2.4'

process.patMuonAnalyzer = cms.EDAnalyzer("PatMuonEDAnalyzer",
  muons = cms.InputTag("cleanPatMuons"), METs = cms.InputTag("patMETs"),  electrons = cms.InputTag("cleanPatElectrons"),     jets = cms.InputTag("cleanPatJets"),                                    
)

process.TFileService = cms.Service("TFileService",
  fileName = cms.string('~/pattuples/patTuple2.root')
)


## let it run
process.p = cms.Path(
    process.patDefaultSequence +
	process.patMuonAnalyzer
    )
