#include "DataFormats/Common/interface/Handle.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/EgammaCandidates/interface/GsfElectron.h"
#include "DataFormats/METReco/interface/MET.h"
#include "DataFormats/JetReco/interface/PFJet.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "PhysicsTools/PatExamples/interface/PatMuonAnalyzer.h"


/// default constructor
PatMuonAnalyzer::PatMuonAnalyzer(const edm::ParameterSet& cfg, TFileDirectory& fs): 
  edm::BasicAnalyzer::BasicAnalyzer(cfg, fs),
  muons_(cfg.getParameter<edm::InputTag>("muons")),
  METs_(cfg.getParameter<edm::InputTag>("METs")),
  electrons_(cfg.getParameter<edm::InputTag>("electrons")),
  jets_(cfg.getParameter<edm::InputTag>("jets"))
{
  std::cout << "creating histos" << std::endl;
  hists_["muonpT"  ] = fs.make<TH1F>("muonpT"  , "pt"  ,  100,  0., 300.);
  hists_["muonEta" ] = fs.make<TH1F>("muonEta" , "eta" ,  100, -3.,   3.);
  hists_["muonPhi" ] = fs.make<TH1F>("muonPhi" , "phi" ,  100, -5.,   5.);
  hists_["muonCharge" ] = fs.make<TH1F>("muonCharge" , "charge" ,  100, -3.,   3.);
  hists_["muonsumChargedHadronPt" ] = fs.make<TH1F>("muonsumChargedHadronPt" , "sumChargedHadronPt" ,  100, -300.,   300.);
  hists_["muonsumNeutralHadronEt" ] = fs.make<TH1F>("muonsumNeutralHadronEt" , "sumNeutralHadronEt" ,  100, -300.,   300.);
  hists_["muonsumPhotonEt" ] = fs.make<TH1F>("muonsumPhotonEt" , "sumPhotonEt" ,  100, -300.,   300.);
  hists_["muonsumPUPt" ] = fs.make<TH1F>("muonsumPUPt" , "sumPUPt" ,  100, -300.,   300.);
  hists_["METpT"   ] = fs.make<TH1F>("METpT"  , "MET"  ,  100,  0., 300.);
  hists_["METPhi" ] = fs.make<TH1F>("METPhi" , "phi" ,  100, -5.,   5.);
  hists_["electronpT"   ] = fs.make<TH1F>("electronpT"  , "pt"  ,  100,  0., 300.);
  hists_["electronEta" ] = fs.make<TH1F>("electronEta" , "eta" ,  100, -3.,   3.);
  hists_["electronPhi" ] = fs.make<TH1F>("electronPhi" , "phi" ,  100, -5.,   5.);
  hists_["electronCharge" ] = fs.make<TH1F>("electronCharge" , "charge" ,  100, -3.,   3.);
  hists_["electronsumChargedHadronIso" ] = fs.make<TH1F>("electronsumChargedHadronIso" , "sumChargedHadronIso" ,  100, -300.,   300.);
  hists_["electronsumNeutralHadronIso" ] = fs.make<TH1F>("electronsumNeutralHadronIso" , "sumNeutralHadronIso" ,  100, -300.,   300.);
  hists_["electronsumPhotonIso" ] = fs.make<TH1F>("electronsumPhotonIso" , "sumPhotonIso" ,  100, -300.,   300.);
 /// hists_["electronsumPUIso" ] = fs.make<TH1F>("electronsumPUIso" , "sumPUIso" ,  100, -300.,   300.);
  hists_["jetpT"  ] = fs.make<TH1F>("jetpT"  , "pT"  ,  100,  0., 300.);
  hists_["jetEta" ] = fs.make<TH1F>("jetEta" , "eta" , 100, -3.,   3.);
  hists_["jetPhi" ] = fs.make<TH1F>("jetPhi" , "phi" ,  100, -5.,   5.);
  hists_["jetpartonFlavour" ] = fs.make<TH1F>("jetpartonFlavour" , "partoflavour" ,  100, -3,   3.);
  hists_["jetbDiscriminator1" ] = fs.make<TH1F>("jetbDiscriminator1" , "bDiscriminator1" ,  100, -300.,   300.);




 /// std::cout << "finished creating histos" << std::endl;
}

/// everything that needs to be done during the event loop
void 
PatMuonAnalyzer::analyze(const edm::EventBase& event)
{
 /// std::cout << "clearing vectors..";
  muon_pT->clear();
  muon_phi->clear();
  muon_eta->clear();
  muon_charge->clear();
  muon_sumChargedHadronPt->clear();
  muon_sumNeutralHadronEt->clear();
  muon_sumPhotonEt->clear();
  muon_sumPUPt->clear();
  electron_pT->clear();
  electron_phi->clear();
  electron_eta->clear();
  electron_charge->clear();
  electron_chargedHadronIso->clear();
  electron_neutralHadronIso->clear();
  electron_photonIso->clear();
  jet_pT->clear();
  jet_eta->clear();
  jet_phi->clear();
  jet_partonFlavour->clear();
  jet_bDiscriminator->clear(); 
  // define what muon you are using; this is necessary as FWLite is not 
  // capable of reading edm::Views
  ///std::cout << "done." << std::endl;
  using pat::Muon;
  using pat::MET;
  using pat::Electron;
  using pat::Jet;
  // Handle to the muon collection
  //std::cout << "Creating handles...";
  edm::Handle<std::vector<Muon> > muons;
  event.getByLabel(muons_, muons);
  edm::Handle<std::vector<Electron> > electrons;
  event.getByLabel(electrons_, electrons);
  edm::Handle<std::vector<MET> > METs;
  event.getByLabel(METs_, METs);
  edm::Handle<std::vector<Jet> > jets;
  event.getByLabel(jets_, jets);
  //std::cout << "done." << std::endl;

  // loop muon collection and fill histograms
 //std::cout << "loop fo muons...";
 for(std::vector<Muon>::const_iterator mu1=muons->begin(); mu1!=muons->end(); ++mu1){
    muon_pT->push_back( mu1->pt() );
    muon_phi->push_back( mu1->phi() );
    muon_eta->push_back( mu1->eta() );
    muon_charge->push_back( mu1->charge() );
    muon_sumChargedHadronPt->push_back( mu1->pfIsolationR04().sumChargedHadronPt  );
    muon_sumNeutralHadronEt->push_back( mu1->pfIsolationR04().sumNeutralHadronEt  );
    muon_sumPhotonEt->push_back( mu1->pfIsolationR04().sumPhotonEt );
    muon_sumPUPt->push_back( mu1->pfIsolationR04().sumPUPt );
    hists_["muonpT" ]->Fill( mu1->pt () );
    hists_["muonEta"]->Fill( mu1->eta() );
    hists_["muonPhi"]->Fill( mu1->phi() );
    hists_["muonCharge"]->Fill( mu1->charge() );
    hists_["muonsumChargedHadronPt"]->Fill( mu1->pfIsolationR04().sumChargedHadronPt );
    hists_["muonsumNeutralHadronEt"]->Fill( mu1->pfIsolationR04().sumNeutralHadronEt );
    hists_["muonsumPhotonEt"]->Fill( mu1->pfIsolationR04().sumPhotonEt );
    hists_["muonsumPUPt"]->Fill( mu1->pfIsolationR04().sumPUPt );
 }

 //std::cout << "done" << std::endl;
 //std::cout << "loop for METs...";
 for(std::vector<MET>::const_iterator met1=METs->begin(); met1!=METs->end(); ++met1){
    MET_pT = met1->pt();
    MET_phi = met1->phi();
    hists_["METpT"]->Fill( met1->pt  () );
    hists_["METPhi"]->Fill( met1->phi () );
 }
 //std::cout << "done" << std::endl;
 //std::cout << "loop for electrons...";
 for(std::vector<Electron>::const_iterator elec1=electrons->begin(); elec1!=electrons->end(); ++elec1){
    electron_pT->push_back( elec1->pt() );
    electron_phi->push_back( elec1->phi() );
    electron_eta->push_back( elec1->eta() );
    electron_charge->push_back( elec1->charge() );
    electron_chargedHadronIso->push_back( elec1->pfIsolationVariables().chargedHadronIso  );
    electron_neutralHadronIso->push_back( elec1->pfIsolationVariables().neutralHadronIso  );
    electron_photonIso->push_back( elec1->pfIsolationVariables().photonIso  );
    hists_["electronpT"]->Fill( elec1->pt  () );
    hists_["electronEta"]->Fill( elec1->eta() );
    hists_["electronPhi"]->Fill( elec1->phi() );
    hists_["electronCharge"]->Fill( elec1->charge() );
    hists_["electronsumChargedHadronIso"]->Fill( elec1->pfIsolationVariables().chargedHadronIso );
    hists_["electronsumNeutralHadronIso"]->Fill( elec1->pfIsolationVariables().neutralHadronIso );
    hists_["electronsumPhotonIso"]->Fill( elec1->pfIsolationVariables().photonIso );
   /// hists_["electronsumPUIso"]->Fill( elec1->pfIsolationVariables().pUIso );

 }
 for(std::vector<Jet>::const_iterator jt1=jets->begin(); jt1!=jets->end(); ++jt1){
    jet_pT->push_back( jt1->pt() );
    jet_eta->push_back( jt1->eta() );
    jet_phi->push_back( jt1->phi() );
    jet_partonFlavour->push_back( jt1->partonFlavour() );
    jet_bDiscriminator->push_back( jt1->bDiscriminator("trackCountingHighPurBJetTags") );
    hists_["jetpT"]->Fill( jt1->pt  () );
    hists_["jetEta"]->Fill( jt1->eta() );
    hists_["jetPhi"]->Fill( jt1->phi() );
    hists_["jetpartonFlavour"]->Fill( jt1->partonFlavour() );
    hists_["jetbDiscriminator1"]->Fill( jt1->bDiscriminator("trackCountingHighPurBJetTags") );
    std::cout<<jt1->bDiscriminator("")<<std::endl;
 }

 //std::cout << "done" << std::endl;
 ///std::cout << "Filling tree...";
 t_out->Fill();
 //std::cout << "done." << std::endl;
}
