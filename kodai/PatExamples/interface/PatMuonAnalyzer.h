#include <map>
#include <string>
#include "TH1.h"
#include "PhysicsTools/UtilAlgos/interface/BasicAnalyzer.h"
#include "TFile.h"
#include "TTree.h"
/**
   \class PatMuonAnalyzer PatMuonAnalyzer.h "PhysicsTools/PatExamples/interface/PatMuonAnalyzer.h"
   \brief Example class that can be used to analyze pat::Muons both within FWLite and within the full framework

   This is an example for keeping classes that can be used both within FWLite and within the full 
   framework. The class is derived from the BasicAnalyzer base class, which is an interface for 
   the two wrapper classes EDAnalyzerWrapper and FWLiteAnalyzerWrapper. You can fin more information 
   on this on WorkBookFWLiteExamples#ExampleFive.
*/

class PatMuonAnalyzer : public edm::BasicAnalyzer {

 public:
  /// default constructor
  PatMuonAnalyzer(const edm::ParameterSet& cfg, TFileDirectory& fs);
  /// default destructor
  virtual ~PatMuonAnalyzer(){};
  /// everything that needs to be done before the event loop
  void beginJob(){
    muon_pT = new std::vector<double>;
    muon_phi = new std::vector<double>;
    muon_eta = new std::vector<double>;
    muon_charge = new std::vector<double>;
    muon_sumChargedHadronPt = new std::vector<double>;
    muon_sumNeutralHadronEt = new std::vector<double>;
    muon_sumPhotonEt = new std::vector<double>;
    muon_sumPUPt = new std::vector<double>;
    electron_pT = new std::vector<double>;
    electron_phi = new std::vector<double>;
    electron_eta = new std::vector<double>;
    electron_charge = new std::vector<double>;
    electron_chargedHadronIso = new std::vector<double>;
    electron_neutralHadronIso = new std::vector<double>;
    electron_photonIso = new std::vector<double>;
    jet_pT = new std::vector<double>;
    jet_eta = new std::vector<double>;
    jet_phi = new std::vector<double>;
    jet_partonFlavour = new std::vector<double>;
    jet_bDiscriminator = new std::vector<double>;
    std::cout << "Creating file...";
    f_out = new TFile("OUTFILE.root", "RECREATE");
    f_out->cd();
    std::cout << "done." << std::endl;
    std::cout << "Creating tree...";
    t_out = new TTree("TREE", "TREE");
    t_out->Branch("muon_pT", &muon_pT);
    t_out->Branch("muon_phi", &muon_phi);
    t_out->Branch("muon_eta", &muon_eta);
    t_out->Branch("muon_charge", &muon_charge);
    t_out->Branch("muon_sumChargedHadronPt", &muon_sumChargedHadronPt);
    t_out->Branch("muon_sumNeutralHadronEt", &muon_sumNeutralHadronEt);
    t_out->Branch("muon_sumPhotonEt", &muon_sumPhotonEt);
    t_out->Branch("muon_sumPUPt", &muon_sumPUPt);
    t_out->Branch("MET_pT", &MET_pT);
    t_out->Branch("MET_phi", &MET_phi);
    t_out->Branch("electron_pT", &electron_pT);
    t_out->Branch("electron_phi", &electron_phi);
    t_out->Branch("electron_eta", &electron_eta);
    t_out->Branch("electron_charge", &electron_charge);
    t_out->Branch("electron_chargedHadronIso", &electron_chargedHadronIso);
    t_out->Branch("electron_neutralHadronIso", &electron_neutralHadronIso);
    t_out->Branch("electron_photonIso", &electron_photonIso);
    t_out->Branch("jet_pT", &jet_pT);
    t_out->Branch("jet_eta", &jet_eta);
    t_out->Branch("jet_phi", &jet_phi);
    t_out->Branch("jet_partonFlavour", &jet_partonFlavour);
    t_out->Branch("jet_bDiscriminator", &jet_bDiscriminator);
    std::cout << "done." << std::endl;
    std::cout << "Setup finished.\n" << std::endl;
  };
  /// everything that needs to be done after the event loop
  void endJob(){
    std::cout << "Writing to file...";
    f_out->cd();
    t_out->Write();
    f_out->Close();
    std::cout << "done.\nWrap-up finished. Exiting." << std::endl;
  };
  /// everything that needs to be done during the event loop
  void analyze(const edm::EventBase& event);

 private:
  /// input tag for mouns
  edm::InputTag muons_;
  edm::InputTag METs_;
  edm::InputTag electrons_;
  edm::InputTag jets_;
/// histograms
  std::map<std::string, TH1*> hists_;

  TFile *f_out;
  TTree *t_out;
  std::vector<double> *muon_pT;
  std::vector<double> *muon_phi;
  std::vector<double> *muon_eta;
  std::vector<double> *muon_charge;
  std::vector<double> *muon_sumChargedHadronPt;
  std::vector<double> *muon_sumNeutralHadronEt;
  std::vector<double> *muon_sumPhotonEt;
  std::vector<double> *muon_sumPUPt;
  double MET_pT;
  double MET_phi;
  std::vector<double> *electron_pT;
  std::vector<double> *electron_phi;
  std::vector<double> *electron_eta;
  std::vector<double> *electron_charge;
  std::vector<double> *electron_chargedHadronIso;
  std::vector<double> *electron_neutralHadronIso;
  std::vector<double> *electron_photonIso;
  std::vector<double> *jet_pT;
  std::vector<double> *jet_eta;
  std::vector<double> *jet_phi;
  std::vector<double> *jet_partonFlavour;
  std::vector<double> *jet_bDiscriminator;
};
